(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(gruvbox-dark-hard))
 '(custom-safe-themes
   '("6b5c518d1c250a8ce17463b7e435e9e20faa84f3f7defba8b579d4f5925f60c1" default))
 '(package-selected-packages '(magit evil gruvbox-theme)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Fira Code" :foundry "CTDB" :slant normal :weight normal :height 111 :width normal)))))

;; evil mode
(require 'evil)
(evil-mode 1)

;; disable menu bar, tool bar and scroll bar
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; set backup file directory
;;(setq backup-directory-alist '(("." . ,(concat user-emacs-directory "backups"))))

;; don't show initial emacs screen and set the scratch message to be empty
(setq inhibit-startup-message t initial-scratch-message "")

;; interactively do things mode
(require 'ido)
(ido-mode)
(ido-everywhere)
(setq ido-enable-flex-matching t)

;; enable column number display in mode line
(column-number-mode)
;; display line numbers on the left side
(global-display-line-numbers-mode)
(setq display-line-numbers-type 'relative)
;; immediately highlight matching parenthesis
(setq show-paren-delay 0)
(show-paren-mode)

;; highlight extra whitespace
(setq-default show-trailing-whitespace t)
(setq-default indicate-empty-lines t)
(setq-default indicate-buffer-boundaries 'left)


;; julia mode
(require 'julia-mode)
