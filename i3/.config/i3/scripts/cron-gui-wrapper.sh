#!/bin/sh
# This wrapper script can be used to enable notify-send in scripts called by cron
# The script first exports the values of DBUS_SESSOIN_BUS_ADDRESS and DISPLAY
# and then sources the script given in the first parameter
# Example crontab usage:
# */15    12-19   *   *   *   $HOME/.config/i3/scripts/cron-gui-wrapper.sh $HOME/borg/backup.sh
environ="/proc/$(pgrep -x i3 | head -n 1)/environ"
DBUS_SESSION_BUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS $environ | sed 's/DBUS_SESSION_BUS_ADDRESS=//')
DISPLAY=$(grep -z DISPLAY $environ | sed 's/DISPLAY=//')
export DBUS_SESSION_BUS_ADDRESS
export DISPLAY
. $1
