#!/bin/sh
# open a file in zathura and fork to the background
pdffile=$(fd --no-ignore-vcs -Hi -e pdf . $HOME | fzf) && zathura --fork "$pdffile"
