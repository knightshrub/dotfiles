#!/bin/sh
function dmenu_prompt {
    dmenu -b -i\
        -p $1\
        -fn 'Fira Mono-12'\
        -nb '#1d2021'\
        -nf '#ebdbb2'\
        -sb '#fe8019'\
        -sf '#1d2021'
}
# generate TOTP codes using the yubikey and copy them to the clipboard
account=$(ykman oath accounts list | dmenu_prompt 'Account:')
ykman oath accounts code $account | awk '{print $2}' | xclip -i
