# dotfiles

Dotfiles are installed using [GNU Stow](https://www.gnu.org/software/stow/) and version controlled using [git](https://git-scm.org/).

## Installation

Because of the default behaviour of stow, installing dotfiles
is a simple as running
```
~ $ git clone https://gitlab.com/knightshrub/dotfiles .dotfiles
~ $ cd .dotfiles
.dotfiles $ stow *
```

## Restic

The restic backup is set up as a generic systemd user unit.
In order to define the pass phrase to access the repo,
create an unit override using
```
$ systemctl --user edit restic
```
with contents
```
[Service]
Environment='RESTIC_PASSWORD=supersecret'
```
Additionally define the `restic-host` in `~/.ssh/config`
```
Host restic-host
    User user
    HostName host
    Port port
    IdentityFile /home/knightshrub/.ssh/restic_ed25519
    ServerAliveInterval 60
    ServerAliveCountMax 240
```
Machine specific excludes are defined in `restic.exclude`.
Enable the service using `systemctl --user enable --now restic.timer`.

## Borg

The borg backup is set up as a generic systemd user unit.
In order to define the pass phrase, host and command 
to access the repo, create an unit override using
```
$ systemctl --user edit borg
```
with contents
```
[Service]
Environment="BORG_RSH=ssh -p 23 -i %h/.ssh/borg_ed25519"
Environment="BORG_REPO=ssh://user@host/path"
Environment="BORG_PASSPHRASE=supersecret"
```
Machine specific excludes are defined in `borg.patterns`.
Enable the service using `systemctl --user enable --now borg.timer`.


## Versioning

The git repository structure works as follows:

- The `master` branch contains configuration options common to
all machines

- configuration file changes specific to machine `hostname` are committed on a branch named `hostname`, which is then rebased onto `master` 

Use `git rebase -i master` to start an interactive rebase onto master.
The commits added to the end of the branch can be moved to master by the following
rebase stack
```
pick M2 Update file1 and file2
pick M1 Update file3
exec git branch -f master
pick C3 Commit message for C3
pick C2 Commit message for C2
pick C1 Commit message for C1
```

Pushing to master without checking it out:
```
git push origin master:master
```
