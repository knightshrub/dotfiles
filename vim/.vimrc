set tabstop=4 shiftwidth=0 expandtab
set nu rnu
set ruler
set ai
set colorcolumn=80
syntax on
set t_Co=256
set background=dark
let g:gruvbox_contrast_dark='hard'
colorscheme gruvbox
hi Normal guibg=NONE ctermbg=NONE
