#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# color outputs on some common comands
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias pacman='pacman --color=auto'

# set a nice terminal prompt
PS1='\[\e[37m\][\[\e[1;37m\]\u\[\e[0;37m\]@\[\e[37m\]\h \[\e[1;33m\]\W\[\e[0;37m\]]\$\[\e[0m\] '

# add $HOME/bin to the $PATH if it exists
if [ -d $HOME/bin ]; then
	export PATH=$PATH:$HOME/bin
fi

# colorized man pages
man() {
    LESS_TERMCAP_md=$'\e[01;36m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[0;30;47m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01m' \
    command man "$@"
}

# make sure things work on wayland
#export MOZ_ENABLE_WAYLAND=1
#export QT_QPA_PLATFORM=wayland-egl
#export QT_WAYLAND_FORCE_DPI=150

# themes for GTK2
export GTK2_RC_FILES=$HOME/.gtkrc-2.0

# without the environment variable firefox UI is unbearably small
alias firefox='GDK_DPI_SCALE=1.3 firefox'
alias thunderbird='GDK_DPI_SCALE=1.3 thunderbird'

# surprise! all GTK apps really need to be scaled up to be usable
alias kicad='GDK_DPI_SCALE=1.5 kicad'

# I usually want to run qemu-x86_64 with kvm enabled
# this means that the user has to be part of the kvm group
alias qemu='qemu-system-x86_64 -enable-kvm'

# the default modifier for baudline is alt
# and conflicts with my sway configuration
alias baudline='baudline -modifier ctrl'

# matlab doesn't play nice with sway
alias matlab='_JAVA_AWT_WM_NONREPARENTING=1 matlab'

# generate TOTP codes using the yubikey and copy them to the clipboard
function totp {
    ykman oath code $1 | awk '{print $2}' | xclip -i
}

# open file with zathura, either the one passed in or launch fzf
function pdfo {
    if [ -z $1 ]; then
        zathura "$(fzf)" &
    else
        zatura $1 &
    fi
}

# make sure ssh-add can talk to gpg
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)

# let the GPG agent know on which tty for pin entry
export GPG_TTY=$(tty)
#gpg-connect-agent updatestartuptty /bye > /dev/null

function start_sway {
    export GTK_IM_MODULE=ibus
    export QT_IM_MODULE=ibus
    export XMODIFIERS=@im=ibus
    ibus-daemon -drx &
    exec sway
}

# auto start sway on tty1
if [ "$(tty)" = "/dev/tty1" ]; then
    exec startx
fi

export PATH="$HOME/.cargo/bin:$PATH"
export PATH="/home/knightshrub/texlive/bin/x86_64-linux:$PATH"
export PATH="/home/knightshrub/MATLAB/R2019b/bin:$PATH"
export PATH="/home/knightshrub/msp430-gcc/bin:$PATH"
source "/home/knightshrub/.local/cargo/env"
