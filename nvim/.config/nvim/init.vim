if !exists('g:vscode')
    set runtimepath^=~/.vim runtimepath+=~/.vim/after
    let &packpath = &runtimepath
    source ~/.vimrc
endif

call plug#begin()

Plug 'alaviss/nim.nvim'
Plug 'JuliaEditorSupport/julia-vim'

call plug#end()
