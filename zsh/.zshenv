typeset -U PATH path
path=("$HOME/.local/cargo/bin" "$HOME/.local/texlive/2021/bin/x86_64-linux" "$HOME/.local/bin" "$HOME/.local/scripts" "$HOME/.local/MATLAB/R2020a/bin" "$HOME/.local/julia/bin" "$path[@]")
EDITOR=nvim
export PATH
export EDITOR

export CARGO_HOME="$HOME/.local/cargo"
export RUSTUP_HOME="$HOME/.local/rustup"

export GNUPGHOME="$HOME/.config/gnupg"
