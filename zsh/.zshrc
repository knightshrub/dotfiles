# The following lines were added by compinstall
#zstyle ':completion:*' completer _complete _ignored _approximate
#zstyle :compinstall filename '/home/knightshrub/.zshrc'

zstyle ':completion:*' menu select

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -v
# End of lines configured by zsh-newuser-install

autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git
precmd() {
    vcs_info
}

zstyle ':vcs_info:git*' formats " (%F{green}%B %b%%b%F{white})"
zstyle ':vcs_info:git*' actionformats " (%F{green}%B %b %F{white}| %F{red} %a%%b%F{white})"

# set a nice terminal prompt
setopt prompt_subst
PS1='%F{white}%n %F{cyan}%B%c%b%F{white}${vcs_info_msg_0_} $%f '


# Use vim keys in tab complete menu:
#bindkey -M menuselect 'h' vi-backward-char
#bindkey -M menuselect 'k' vi-up-line-or-history
#bindkey -M menuselect 'l' vi-forward-char
#bindkey -M menuselect 'j' vi-down-line-or-history
#bindkey -v '^?' backward-delete-char

source /usr/share/fzf/completion.zsh
export FZF_COMPLETION_TRIGGER='\\'
export FZF_COMPLETION_OPTS="--height 90% --min-height 5 --layout reverse --border none --preview 'head -n $LINES {}'"

_fzf_compgen_path() {
    fd --hidden --follow --exclude ".git" . "$1"
}

_fzf_compgen_dir() {
    fd --type d --hidden --follow --exclude ".git" . "$1"
}

# colorized man pages
man() {
    LESS_TERMCAP_md=$(printf '\e[01;34m') \
    LESS_TERMCAP_me=$(printf '\e[0m') \
    LESS_TERMCAP_se=$(printf '\e[0m') \
    LESS_TERMCAP_so=$(printf '\e[0;30;47m') \
    LESS_TERMCAP_ue=$(printf '\e[0m') \
    LESS_TERMCAP_us=$(printf '\e[04;33m') \
    command man "$@"
}

# define exported variables
if [[ -f $HOME/.exportrc ]]; then source $HOME/.exportrc; fi
# define aliases
if [[ -f $HOME/.aliasrc ]]; then source $HOME/.aliasrc; fi

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/knightshrub/.local/anaconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/knightshrub/.local/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/home/knightshrub/.local/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/knightshrub/.local/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
